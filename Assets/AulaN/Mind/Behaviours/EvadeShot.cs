﻿using UnityEngine;
using System.Collections;

public class EvadeShot : AbstractBehaviour 
{
	public Vector3 t_direction = Vector3.zero;

	public override void Act()
	{
		foreach (Bullet blt in owner.target.GetComponent<Mind>().bullets)
		{
			if(blt.damage>0){

				if (CanAct(blt))
				{
					owner.body.Move(SolveDirection(-blt.direction));
					//owner.body.Move(t_direction);
				}
			}

		}
	}

	public override bool Think()
	{
		/*if (owner.target != null)
			return true;
		return false;
*/
		//print (owner.target.GetComponent<Mind>().bullets.Count);

		if(owner.target != null){
			if(owner.target.GetComponent<Mind>().bullets.Count == 0){return false;}

			foreach (Bullet blt in owner.target.GetComponent<Mind>().bullets)
			{
				if(blt != null){
					if(blt.damage>0){
						if(CanAct(blt)){return true;}
					}
				}
				
			}
		}
		return false;

	}

	public bool CanAct(Bullet bullet)
	{
		//Testes para verificar se o objeto ira reagir ao bullet
		if (bullet != null)
		{
			if (bullet.bullet_owner.name != owner.name)
			{
				if (Vector3.Distance(bullet.transform.position, transform.position) < 2f)
				{
					return true;
				}
			}
		}
		return false;
	}

	/*public Vector3 SolveDirection(Vector3 m_position, Vector3 blt_position)
	{
		if (m_position.y <= (blt_position.y + 3.0f))
		{
			return m_position = new Vector3(m_position.x,
														m_position.y + 5.0f * Time.deltaTime,
														m_position.z);
		}
		else if (m_position.y >= (blt_position.y - 3.0f))
		{
			return m_position = new Vector3(m_position.x,
														m_position.y - 5.0f * Time.deltaTime,
														m_position.z);
		}
		else if (m_position.x >= (blt_position.x - 3.0f))
		{
			return m_position = new Vector3(m_position.x - 5.0f * Time.deltaTime,
														m_position.y,
														m_position.z);
		}
		else if (m_position.x <= (blt_position.x + 3.0f))
		{
			return m_position = new Vector3(m_position.x + 5.0f * Time.deltaTime,
														m_position.y,
														m_position.z);
		}
		else
			return m_position;
	}*/

	public Vector3 SolveDirection(Vector3 bullet_direction)
	{
		/*m_direction = new Vector3(Mathf.Cos(t_ang), 
																			Mathf.Sin(t_ang), 
																			0.0f);*/
		/*Vector3 m_direction = new Vector3(bullet_direction.x * Mathf.Cos(Mathf.PI / 3),
																			bullet_direction.y * -Mathf.Sin(Mathf.PI / 2),
																			0.0f);*/

		Vector3 m_direction = new Vector3(-bullet_direction.y,
																			bullet_direction.x,
																			0.0f);
//		Debug.Log(m_direction);
		t_direction = m_direction;
		return m_direction;
	}
}
