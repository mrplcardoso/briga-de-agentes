﻿using UnityEngine;
using System.Collections;

public class FollowTarget : AbstractBehaviour 
{

	// Use this for initialization
	public override void Act()
	{
		//if(owner.target != null){

			Body enemy = owner.target;

			//enemy.renderer.material.color = Color.yellow;
		
			if(Vector3.Distance(enemy.transform.position, transform.position ) > 5)
			{
				owner.body.Move((enemy.transform.position - transform.position).normalized);
				//GoToAttacker(owner.target);//@ ou usar o FollowTarget
			}
		//}
	}



// Update is called once per frame
	public override bool Think()
	{

		if(owner.target != null){

			if(owner.myType == Type.Defender){
				if(owner.ally_target != null){return false;}
			}
			
			Body enemy = owner.target;
			
			//enemy.renderer.material.color = Color.yellow;
			
			if(Vector3.Distance(enemy.transform.position, transform.position ) <= 5)
			{
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
}
