using UnityEngine;
using System.Collections;

public class SearchBulletInHealerDirection : AbstractBehaviour
{
	Bullet b_target;
	float current_distance = 0.0f;
	Body ally;
	
	public override void Act()
	{
		if(b_target != null){
			owner.body.Move((b_target.transform.position - transform.position).normalized);
			//b_target.renderer.material.color = Color.red;
		}

	}
	
	public override bool Think()
	{
		/*for (int j = 0; j < owner.allies.Count; j++)
		{
			if (owner.allies[j].GetComponent<Mind>().myType == Type.Healer){
				if (owner.allies[j].life > 0.0f || owner.allies != null){
					print ("passou11111");return true;
				}
			}
		}
		return false;*/
		



		int i = 0;//contador
		
		///VERIFICAR POIS AQUI ELE SOH SABE O MOMENTO QUE A BALA SAIU DO INIMIGO E VERIFICA
		/// APENAS NESSE MOMENTO, CASO O HEALER ENTRAR NA FRENTE DA BALA OU SAIR DA FRENTE DA BALA,
		/// A ACAO TOMADA PELO DEFENDER NAO TERA ALTERACAO... VER COM O MARCOS!!!
		foreach(Bullet enemy_bullet in owner.enemies[i].GetComponent<Mind>().bullets)
		{
			for (int j = 0; j < owner.allies.Count; j++)
			{
				if (owner.allies[j].GetComponent<Mind>().myType == Type.Healer)
				{
					ally = owner.allies[j];
					owner.ally_target = ally;
				}
			}
			if (ally != null)
			{
				if (enemy_bullet != null)
				{//aperfeiçoar esta funçao!!!
					if( (enemy_bullet.direction) == 
					   ((ally.transform.position - enemy_bullet.transform.position).normalized) /*&& 
					    (enemy_bullet.direction) < 
						-((ally.transform.position + 0.5 - enemy_bullet.transform.position).normalized)*/
					   )
					{	
						enemy_bullet.renderer.material.color = Color.magenta;
						current_distance = Vector3.Distance(enemy_bullet.transform.position, ally.transform.position);
						
						if(b_target == null){b_target = enemy_bullet; owner.follow_target = b_target;/* @ FAZER FUNÇAO DE SEGUIR A BALA COM O PURSUIT */}
						else 
						{
							if(current_distance < Vector3.Distance(b_target.transform.position, ally.transform.position))
							{b_target = enemy_bullet; owner.follow_target = b_target;/* @ FAZER FUNÇAO DE SEGUIR A BALA COM O PURSUIT*/}
							//owner.target = enemy_bullet;
							//owner.target.body.Move((owner.target.transform.position - transform.position).normalized);
							//owner.body.Move((b_target.transform.position - transform.position).normalized);

						}
					}
					else{
						//b_target = enemy_bullet;
						//b_target = null;
						//owner.body.Move((b_target.transform.position - transform.position).normalized);
						enemy_bullet.renderer.material.color = Color.white;
					}
				}
			}
			else{owner.ally_target = null; return false;}
			i++;
		}
		
		if(b_target != null){
			//owner.body.Move((b_target.transform.position - transform.position).normalized);
			b_target.renderer.material.color = Color.red;
			return true;
		}
		owner.follow_target = null;
		return false;
	}
}