﻿using UnityEngine;
using System.Collections;

public class ReturnToDef : AbstractBehaviour {

	public override void Act(){
		
		foreach (Body ally in owner.allies) {

			if(ally.GetComponent<Mind>().myType == Type.Healer && ally.life > 0)
			{
				owner.ally_target = ally;
			
				if(Vector3.Distance(owner.ally_target.transform.position, transform.position ) >= 1.5f)
				{
					owner.body.Move((owner.ally_target.transform.position - transform.position).normalized);
					break;
				}
				
			}
		}
		
	}
	
	public override bool Think(){
		
		foreach (Body ally in owner.allies) {
			
			if(ally.GetComponent<Mind>().myType == Type.Healer && ally.life > 0)
			{
				owner.ally_target = ally;
				
				if(Vector3.Distance(owner.ally_target.transform.position, transform.position ) >= 1.5f)
				{
					return true;
				}			
			}
		}

		return false;

		
	}
}
