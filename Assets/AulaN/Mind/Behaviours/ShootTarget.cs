﻿using UnityEngine;
using System.Collections;

public class ShootTarget : AbstractBehaviour
{
	public float timer = 1;
	public override void Act()
	{
		if(owner.target != null)
		{
			Vector3 t_direction = (owner.target.transform.position - transform.position).normalized;
			owner.body.Shoot(t_direction);
		}
		if(owner.GetComponent<Mind>().myType == Type.Healer && owner.ally_target != null)
		{
			Vector3 t_direction = (owner.ally_target.transform.position - transform.position).normalized;
			owner.body.Shoot(t_direction);
		}
	}

	public override bool Think()
	{
		//Contador para tiro
		if (timer >= 1.1)
		{
			//Debug.Log("I think Im fucking shooting here");
			timer = 0;
			return true;
		}
		timer += 0.01f;
		return false;
	}
}
