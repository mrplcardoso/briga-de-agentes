﻿using UnityEngine;
using System.Collections;

public class GoToCure : AbstractBehaviour {


	public float time = 0;
	public bool cure = false;
	
	public override void Act(){
		
		foreach (Body ally in owner.allies) {

			if(ally.GetComponent<Mind>().myType == Type.Attacker && ally.life < ally.initLife * 0.8)
			{
				owner.ally_target = ally;

				if(Vector3.Distance(ally.transform.position, transform.position ) > 3f)
				{
					owner.body.Move((owner.ally_target.transform.position - transform.position).normalized);
					//GoToAttacker(owner.target);//@ ou usar o FollowTarget
					break;
				}
				break;
				
			}
			if(ally.GetComponent<Mind>().myType == Type.Defender && ally.life < ally.initLife * 0.8)
			{
				owner.ally_target = ally;

				if(Vector3.Distance(ally.transform.position, transform.position ) > 3f)
				{
					owner.body.Move((owner.ally_target.transform.position - transform.position).normalized);
					//GoToAttacker(owner.target);
					break;
				}
				break;
			}

			owner.ally_target = null;
			
		}
		
	}
	
	public override bool Think(){

		return true;

		/*if (time >= 1 && !cure) {
			
			cure = true;
			return true;
		}
		time +=  0.1f;
		return false;*/
		
	}

}
