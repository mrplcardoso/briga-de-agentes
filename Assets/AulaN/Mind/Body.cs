//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEngine;
		
public class Body : MonoBehaviour
{
	public float life = 5;
	public float initLife = 5;
	//initLife = life;
	public float speed;
	public float damage;
	public float actionVision = 3.0f;
	public GameObject bullet;


	public float cooldown = 1;
	public float cooldownTimer;

	public void Move(Vector3 direction)
	{
		if (direction != Vector3.zero)
		{
			float stepX = direction.x * Time.deltaTime * speed;
			float stepY = direction.y * Time.deltaTime * speed;
			
			transform.position = new Vector3(transform.position.x + stepX, transform.position.y + stepY, 0);
		}
	}

	public bool CanShoot()
	{
		return (cooldownTimer < Time.time);
	}

	public void Shoot(Vector3 direction)
	{
		if (cooldownTimer < Time.time) 
		{
			Bullet b = (GameObject.Instantiate (bullet, gameObject.transform.position, Quaternion.identity) as GameObject).GetComponent<Bullet> ();
			b.Shoot(GetComponent<Body>(), direction);
			b.damage = this.damage;
			cooldownTimer = Time.time + cooldown;
		}
	}

	public void TakeDamage(float damage)
	{
		life -= damage;

		if (life < 0)
			Die();
	}

	public void Die()
	{
		//Do something fancy here
		DestroyObject(gameObject);
	}

	public void Update()
	{
		if (Input.GetKey (KeyCode.A)) 
		{
			Shoot (Vector3.one);
		}
	}
}

